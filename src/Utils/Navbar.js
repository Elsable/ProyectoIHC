import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class Navbar extends Component {
  static propTypes = {

  }

  render() {
    return (
      <div>
        <header class="header is-global">
        <div class="is-navbar-container">
            <div class="is-brand">
                <a href="/" class="is-logo">Imperavi</a>
                <a href="#" class="nav-toggle is-push-right-mobile is-shown-mobile icon-kube-menu" data-kube="toggle" data-target="#navbar" data-loaded="true"></a>
            </div>
            <nav id="navbar" class="is-navbar is-push-right is-hidden-mobile">
                <ul>
	

		
			<li><a href="/redactor/">Redactor</a></li>
		

	

		
			<li class="is-parent"><a href="/kube/">Kube</a></li>
		

	

		
			<li><a href="/blog/">Blog</a></li>
		

	

		
			<li><a href="/company/">Company</a></li>
		

	

		
			<li><a href="/account/">My Account</a></li>
		

	
</ul>

            </nav>
        </div>
    </header>
      </div>
    )
  }
}

export default Navbar
